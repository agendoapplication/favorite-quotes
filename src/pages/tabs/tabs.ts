import { Component } from "@angular/core";
import { FavoritesPage } from "../favorites-page/favorites-page";
import { LibraryPage } from "../library-page/library-page";

@Component({
    templateUrl: 'tabs.html'
})
export class TabsPage {
    favoritesPage = FavoritesPage;
    libraryPage = LibraryPage;
}