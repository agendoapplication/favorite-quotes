import { Component, OnInit } from '@angular/core';
import quotes from "../../data/quotes";
import { QuotesPage } from "../quotes-page/quotes-page";
import { Quote } from "../../data/quote.interface";

@Component({
  selector: 'page-library-page',
  templateUrl: 'library-page.html',
})
export class LibraryPage implements OnInit {
  quotesPage = QuotesPage;

  quoteCollection: {category: string, quotes: Quote[], icon: string}[];

  ngOnInit() {
    this.quoteCollection = quotes;
  }
}